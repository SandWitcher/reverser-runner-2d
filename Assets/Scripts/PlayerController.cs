﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Contains("Gravity Beam"))
        {
            GetComponent<Rigidbody2D>().gravityScale *= -1;
            TrashMan.despawnAfterDelay(other.gameObject, 2);
        }
    }
}
