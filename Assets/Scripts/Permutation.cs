﻿using DG.Tweening;
using UnityEngine;

public class Permutation : ScrollingItem
{

    private bool _enabled;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Contains("Player Circle"))
        {
            _enabled = true;
        }

        GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
    }

    private void OnMouseDown()
    {
        if (!_enabled) return;
        
        GameObject player = GameObject.FindWithTag("Player");
        
        player.transform.DOScale(0, 0.2f)
            .OnStart(() => transform.DOScale(0, 0.2f)
                .OnComplete(() =>
                {
                    player.transform.position = transform.position;
                    player.transform.DOScale(1, 0.2f);
                }));
    }

    protected override void OnBecameInvisible()
    {
        base.OnBecameInvisible();
        _enabled = false;
        
        GetComponent<SpriteRenderer>().DOFade(0.5f, 0f);
    }
}
