﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform playerTransform;
    
    public float movementDuration;

    private bool _isTweening;

    private Vector3 _targetPosition;

    private void Awake()
    {
        _targetPosition = playerTransform.position;
    }

    private void LateUpdate()
    {
        if (!Mathf.Approximately(playerTransform.position.x, _targetPosition.x) && !_isTweening)
        {
            float translation = playerTransform.position.x - _targetPosition.x;
            transform.DOMoveX(translation, movementDuration)
                .OnStart(() => _isTweening = true)
                .OnComplete(() => _isTweening = false);
        }
    }
}
