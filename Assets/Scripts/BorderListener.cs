﻿using UnityEngine;

public class BorderListener : MonoBehaviour
{

    public GravityController gravityController;

    private void OnMouseEnter()
    {
        gravityController.SetEnabled(true);
    }

    private void OnMouseExit()
    {
        gravityController.SetEnabled(false);
    }

    private void OnMouseDown()
    {
        gravityController.OnClick();
    }
}
