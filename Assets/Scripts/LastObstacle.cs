﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastObstacle : MonoBehaviour
{
    
    public interface LastObstacleListener
    { 
        void OnVisibilityChanged(bool visible);
    }

    public LastObstacleListener listener;

    private void Awake()
    {
        if (listener == null)
        {
            listener = GetComponentInParent<ScrollingObstacles>();
        }
    }

    private void OnBecameVisible()
    {
        listener.OnVisibilityChanged(true);
    }

    private void OnBecameInvisible()
    {
        listener.OnVisibilityChanged(false);
    }
}
