﻿using UnityEngine;

public class ScrollingObstacles : MonoBehaviour, LastObstacle.LastObstacleListener
{
    private Transform _lastObstacle;

    private void Awake()
    {
        _lastObstacle = transform.GetChild(transform.childCount - 1);
        _lastObstacle.gameObject.AddComponent<LastObstacle>();
    }

    public void OnVisibilityChanged(bool visible)
    {
        if (visible) GameManager.instance.OnLastObstacleVisible();
        if (!visible)
        {
            ScrollingManager.GetInstance().RemoveScrollingItemFromList(GetComponent<ScrollingItem>());
            TrashMan.despawn(gameObject);
        }
    }
}
