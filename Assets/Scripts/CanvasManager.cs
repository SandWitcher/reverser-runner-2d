﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
        
    public static CanvasManager instance;
    
    [Header("Main Canvas")]
    public Canvas mainCanvas;
    
    public GameObject[] mainPanels;
    
    [Header("Game Canvas")]
    public Canvas gameCanvas;

    public TextMeshProUGUI pointsText;
    
    public TextMeshProUGUI timerText;

    public Transform livesParent;

    private Image[] _livesImages;


    private Coroutine _updateTimerCoroutine;
    
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    
    private void OnValidate()
    {
        if (mainCanvas != null && mainPanels?.Length != mainCanvas.transform.childCount)
        {
            Transform t = mainCanvas.transform;
            List<GameObject> list = new List<GameObject>();
            
            for (int i = 0; i < t.childCount; i++)
            {
                list.Add(mainCanvas.transform.GetChild(i).gameObject);    
            }

            mainPanels = list.ToArray();
        }
    }

    #region Main Canvas
    
    public void Show(string panelName)
    {
        GameObject panel = mainPanels.FirstOrDefault(p => p.name.Contains(panelName));

        if (panel == null) throw new Exception($"CanvasManager ~ Panel {panelName} not found");
        
        ShowPanel(panel);
    }

    public void Hide(string panelName)
    {
        GameObject panel = mainPanels.FirstOrDefault(p => p.name.Contains(panelName));

        if (panel == null) throw new Exception($"CanvasManager ~ Panel {panelName} not found");

        HidePanel(panel);
    }
    
    public void HidePanels()
    {
        foreach (var panel in mainPanels)
        {
            if (!panel.name.Contains("Panel")) continue;
            HidePanel(panel);
        }
    }



    private void ShowPanel(GameObject panel)
    {
        if (panel.activeSelf && !DOTween.IsTweening(panel.transform)) return;

        panel.transform.DOKill();

        panel.transform.DOScale(1, 1.5f)
            .OnStart(() =>
            {
                panel.SetActive(true);
            })
            .SetUpdate(true);
    }
    
    private void HidePanel(GameObject panel)
    {
        if (!panel.activeSelf) return;
        
        //if (DOTween.IsTweening(panel.transform))

        panel.transform.DOKill();

        panel.transform.DOScale(0, 1.5f)
            .OnComplete(() =>
            {
                panel.SetActive(false);
            })
            .SetUpdate(true);
    }

    #endregion


    #region Game Canvas

    public void OnHit()
    {
        int i = 0;

        while (DOTween.IsTweening(_livesImages[i]) || _livesImages[i].gameObject.activeSelf) i++;
        
        if (i >= _livesImages.Length) return;
        
        Sequence seq = DOTween.Sequence();
        
        seq.Append(_livesImages[i].transform.DOMoveY(5, 1))
            .Append(_livesImages[i].transform.DOMoveY(-20, 1))
            .Join(_livesImages[i].DOFade(0, 1))
            .Play();
    }

    public void OnSuccess(int points)
    {
        Sequence seq = DOTween.Sequence();

        seq.Append(pointsText.transform.DOScale(1.2f, 0.5f))
            .AppendCallback(() =>
            {
                pointsText.SetText(points.ToString());
            })
            .Append(pointsText.transform.DOScale(1f, 0.5f))
            .Play();
    }

    public void ResetTimer()
    {
        if (_updateTimerCoroutine != null) StopCoroutine(_updateTimerCoroutine);
        _updateTimerCoroutine = StartCoroutine(UpdateTime());
    }
    
    private IEnumerator UpdateTime()
    {
        int time = 0;
        while (!GameManager.instance.IsDead)
        {
            if (!GameManager.instance.IsPlaying)
            {
                yield return null;
            }

            yield return new WaitForSeconds(1);

            time++;
            
            timerText.SetText(TimeSpan.FromSeconds(time).ToString(@"mm\:ss"));
        }
    }
    
    #endregion
}
