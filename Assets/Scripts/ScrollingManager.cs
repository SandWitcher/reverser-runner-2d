﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingManager : MonoBehaviour
{
    private static ScrollingManager _instance;

    private List<ScrollingItem> _scrollingItems;

    public Vector2 scrollingSpeed;
    
    public static ScrollingManager GetInstance()
    {
        if (_instance == null) throw new NullReferenceException();
        
        return _instance;
    }
    
    private void Awake()
    {
        if (_instance == null) _instance = this;
        
        _scrollingItems = new List<ScrollingItem>();
    }
    
    private void FixedUpdate()
    {
        foreach (var item in _scrollingItems)
        { 
            item.rb.velocity = scrollingSpeed;
        }
    }

    
    
    public void AddScrollingItemToList(ScrollingItem item)
    {
        _scrollingItems.Add(item);
    }

    public void RemoveScrollingItemFromList(ScrollingItem item)
    {
        _scrollingItems.Remove(item);
    }
}
