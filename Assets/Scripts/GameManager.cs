﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject[] obstaclesPool;

    [Header("Variables")]
    public Transform spawnTransform;
    
    
    
    // Private fields
    private bool _isPlaying;
    public bool IsPlaying { get; }
    
    private bool _isDead;
    public bool IsDead { get; }
    
    private bool _isStarted;
    public bool IsStarted { get; }
    
    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        CanvasManager.instance.Show("Start Panel");
        Time.timeScale = 0;
        
    }

    // toggle
    public void OnPlayPause(bool play)
    {
        Time.timeScale = play ? 1 : 0;
        
        _isPlaying = play;

        if (!_isPlaying)
        {
            if (_isStarted)
            {
                if (!_isDead)
                {
                    CanvasManager.instance.Show("Pause Panel");
                    
                }
                else
                {
                    CanvasManager.instance.Show("Lose Panel");
                }
            }
            else
            {
                CanvasManager.instance.Show("Start Panel");    
            }
        }
        else
        {
            CanvasManager.instance.HidePanels();
        }
    }

    public void OnStart()
    {
        OnLastObstacleVisible();
        _isPlaying = true;
        _isStarted = true;
        _isDead = false;
        CanvasManager.instance.Show("Play/Pause Button");
        CanvasManager.instance.ResetTimer();
        OnPlayPause(true);
    }
    
    public void OnLastObstacleVisible()
    {
        int r = Random.Range(0, obstaclesPool.Length - 1);
        TrashMan.spawn(obstaclesPool[r], Vector3.right * spawnTransform.position.x);
    }
}
